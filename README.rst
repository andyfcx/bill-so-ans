============================================
Analysis of Bill's Stack Overflow Answers
============================================

About
==========

How to earn reputation (abbr. "rep" hereafter) efficiently on Stack Overflow? The project
analyzes the author's answers and reputation gain `on Stack Overflow <https://stackoverflow.com/users/3218693>`_
between ``2020-09-30`` and ``2020-12-05`` to produce some insight. The main questions
of particular interest are:

- Does a longer answer earn more reputation per question?
- Does a longer answer earn more reputation per character?
- Reputation-wise speaking, is it worth answering a question from a new user with reputation < 15 (i.e. cannot vote up)?

For a quick overview of summary stats:

- The author has earned 3,024 rep by posting 177 answers during 45 active days.
    + 3.93 ans per day, or 67.2 rep per day
- Among the answers,
    + 98 (55.4%) were accepted
    + 40 (22.6%) did not result in rep gain
    + The average rep gain is 17.1 per answer.

N.B. The above stats were obtained on ``2020-12-06``. These figures are
subject to change due to occasional later votes and user removals.


Prerequisites
=====================

- conda (`anaconda, miniconda <https://docs.conda.io/en/latest/miniconda.html>`_ or equivalent)
- `docker <https://docs.master.dockerproject.org/get-docker/>`_
    + Enable `execution with ordinary user <https://docs.master.dockerproject.org/engine/install/linux-postinstall/>`_.
- `docker compose <https://docs.docker.com/compose/install>`_

Test Procedure
=====================

Operate on a ``bash``-like command line interface.


1. Download and restore environment
----------------------------------------------------
.. code-block:: bash

    $ git clone bill-so-ans
    $ cd bill-so-ans
    $ conda create --name bill-so --file requirements.txt
    $ conda activate bill-so


2A. Build and run docker image locally
----------------------------------------------------

To be used when CD/CI tools were not set up.

.. code-block:: bash

    # build docker image
    (bill-so) $ docker build -t test-bill-so

    # create container and run
    (bill-so) $ docker-compose up

And then open `<http://localhost:8050>`_ in the browser.

2B. Direct execution
----------------------------

Not recommended for production use.

.. code-block:: bash

    (bill-so) $ python3 app.py

    # open http://localhost:8050 in the browser
