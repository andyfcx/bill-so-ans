FROM nickgryg/alpine-pandas:latest
COPY . /app
WORKDIR /app

RUN cd /app && apk update \
    && apk add gcc libc-dev linux-headers libxslt-dev libxml2 \
    && pip install --upgrade pip 
RUN pip install -r n.requirements.txt

EXPOSE 8000
#CMD python3 app.py # This uses python development server 

# Production server:
CMD gunicorn --workers=4 app:server -b 0.0.0.0:8000  
# Suggested docker comand :
# docker run -p 8000:8000 <image hash/tag> gunicorn --workers=4 app:server -b 0.0.0.0:8000
# Or simply:
# docker run -p 8000:8000 <image hash/tag>