"""
Dash application for Bill's SO reputation change
"""

import sys
import os
from pathlib import Path
import dash
import dash_core_components as dcc
import dash_html_components as html
import dash_bootstrap_components as dbc
from dash.dependencies import Input, Output
import plotly.express as px
import plotly.graph_objects as go
from datetime import datetime

# locate project base directory
if "__file__" in globals():  # console
    base_dir = Path(__file__).absolute().parents[1]
else:  # interactive
    base_dir = Path(os.getcwd())
# add to path
if str(base_dir) not in sys.path:
    sys.path.append(str(base_dir))

from src.so_answers import So_Answers

# =========================================================
# Parameters
# =========================================================
external_stylesheets = [
    dbc.themes.MINTY,
    "/assets/custom.css"
]

# =========================================================
# Data Preprocessing
# =========================================================
o = So_Answers()
# TODO:
#     maybe I should retrieve all data & apply observation range on raw data
#     otherwise the analysis will be infering from future reputation gain...
o.master_workflow()

# =========================================================
# App Body & Main Layout
# =========================================================

app = dash.Dash(__name__, external_stylesheets=external_stylesheets)
# https://community.plotly.com/t/error-with-gunicorn/8247/12
server = app.server

# main layout
app.layout = html.Div([
    dcc.Location(id='url', refresh=False),
    # main title row
    dbc.Row(
        dbc.Col(
            html.H1("Study of Bill's Stack Overflow Answers")
        )
    ),
    html.Hr(),
    # main row
    dbc.Row([
        # main navigation panel
        dbc.Col(width=1, sm=2, children=[
            html.Div(id="main-nav", children=[
                # Topic list
                html.H2("Topics"),
                html.Hr(),
                dbc.Nav([
                    # NOTE: external_link=True: critical library bug
                    # ref: https://github.com/plotly/dash-core-components/issues/925
                    # workaround: https://stackoverflow.com/questions/60454165
                    dbc.NavLink("Home", active=True, href="/", external_link=True),
                    dbc.NavLink("Activity Summary", href="/activity-summary", external_link=True),
                    dbc.NavLink("Reputation Gain", href="/rep-gain", external_link=True),
                ]),
                # dbc.Button("Home", id="home-button", color="primary", className="mr-1"),
                # dbc.Button("Activity Summary", id="activity-summary-button", color="secondary", className="mr-1"),
                # dbc.Button("Reputation Gain", id="rep-gain-button", color="secondary", className="mr-1"),
                html.Br(),
                html.Hr(),
                html.Br(),
                # date range picker
                html.H4("Set period of analysis:"),
                html.P("Please click on the topic again to refresh."),
                # TODO: (lib bug #1406) the last date cannot be picked!
                # discussion: https://github.com/plotly/dash/issues/1406
                dcc.DatePickerRange(
                    id="date-range-select",
                    min_date_allowed=o.begin_date_str,
                    max_date_allowed=o.end_date_str,
                    start_date=o.begin_date_str,
                    end_date=o.end_date_str,
                    initial_visible_month=o.begin_date_str
                ),
                # dummy output for DatePickerRange callback
                html.Div(id="dummy", children=[]),
            ])
        ]),
        # main display area (blank)
        dbc.Col(width=11, sm=10, children=html.Div(id="main-contents", children=[]))
    ]),
    # TODO: add footer row
    html.Footer("""Chuan-Bin "Bill" Huang, 2021""")
])


# =========================================================
# Homepage
# =========================================================

def div_home():
    """Home page"""
    return html.Div([
        html.H2("Introduction Page"),
        html.P("<Fill in: Introduction Text>")
    ])


# @app.callback(Output("main-contents", "children"),
#               [Input("home-button", "n_clicks")])
# def render_home(n_clicks):
#     """Navigation links: home"""
#     if n_clicks is not None:
#         return div_home()


# =========================================================
# Date Picker
# =========================================================

@app.callback(Output("dummy", "children"),
              [Input("date-range-select", "start_date"),
               Input("date-range-select", "end_date")])
def filtered_analysis(start_date, end_date):
    """Re-analyze against the newly-set date range"""
    start_date = datetime.strptime(start_date, "%Y-%m-%d").replace(tzinfo=o.tz_obj)
    end_date = datetime.strptime(end_date, "%Y-%m-%d").replace(tzinfo=o.tz_obj)
    o.filtered_analysis(start_date, end_date)
    return []


# =========================================================
# Topic: Activity Summary
# =========================================================

def fig_activity_summary():
    return go.Figure(
        data=[go.Table(
            header={
                "values": ["Item", "Value"]
            },
            cells={
                "values": [
                    ["First Activity",
                     "Last Activity",
                     "#Active Days",
                     "#Answers"],
                    [o.first_ans_date,
                     o.last_ans_date,
                     o.n_active_days,
                     o.n_ans]
                ]
            }  # end cells
        )]  # end Table, data
    )  # end Figure


def div_activity_summary():
    """Activity summary page"""
    return html.Div([
        html.H2("Summary of Answer Activity"),
        dcc.Graph(
            id="activity-summary-table",
            figure=fig_activity_summary()
        )  # end Graph
    ])


# @app.callback(Output("main-contents", "children"),
#               [Input("activity-summary-button", "n_clicks")])
# def render_activity_summary(n_clicks):
#     if n_clicks is not None:
#         return div_activity_summary()


# =========================================================
# Topic: Reputation Gain Overview
# =========================================================

def fig_rep_gain():
    return px.bar(
        o.df_rep_gain_count,
        title="Distribution of Reputation Gain",
        x='rep_gain', y='count'
    )


def div_rep_gain():
    """Reputation gain page."""
    return html.Div([
        html.H2("Distibution of Reputation Gain"),
        dcc.Graph(id="rep-gain", figure=fig_rep_gain()),
        html.Div(id="rep-gain-details", children=[
            html.Ul([
                html.Li(f"Total rep gain: {o.rep_total}"),
                html.Li(f"Avg. rep gain per active day: {o.rep_total / o.n_active_days:.1f}"),
                html.Li(f"Avg. rep gain per answer: {o.rep_total / o.n_ans:.1f}"),
                html.Li(f"Zero-gain answers: {o.n_a0}/{o.n_ans} ({o.n_a0 / o.n_ans * 100:.1f}%)"),
                html.Li(f"Accepted answers: {o.n_acc}/{o.n_ans} ({o.n_acc / o.n_ans * 100:.1f}%)")
            ])
        ])  # Div rep-gain-details
    ])  # Div


# @app.callback(Output("main-contents", "children"),
#               [Input("rep-gain-button", "n_clicks")])
# def render_rep_gain(n_clicks):
#     if n_clicks is not None:
#         return div_rep_gain()


# =========================================================
# General Callbacks
# =========================================================

# TODO: check after NavLink malfunction is fixed
@app.callback(Output("main-contents", "children"),
              [Input("url", "pathname")])
def main_routing(pathname):
    """Main routing function (implements main_nav)."""
    if pathname == "/":
        return div_home()
    elif pathname == "/activity-summary":
        return div_activity_summary()
    elif pathname == "/rep-gain":
        return div_rep_gain()
    else:
        pass



# ==============================================
# Utility Functions
# ==============================================


if __name__ == '__main__':
    app.run_server(debug=o.dbg, port=8000, host="0.0.0.0")
